<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" type="image/x-icon" href="favicon.ico">

	<?php if ( env('APP_ENV') == 'local') : ?>
		<title>DEV - Home</title>
	<?php else: ?>
		<title>Home</title>
	<?php endif; ?>

	<link rel="stylesheet" href="assets/datasigh/icons/all.min.css">
  <link rel="stylesheet" href="assets/datasigh/css/libs/bootstrap.min.css">
	<?php print asset('datasigh/css/datasigh.css') ?>
	<?php print asset('css/app.css') ?>
	<noscript>
		<strong style="display:block;text-align: center;font-size:26px;margin-top:5rem">Por favor, habilite o JavaScript de seu navegador para continuar na aplicação.</strong>
	</noscript>
</head>
<body>
	<div id="app" class="ds-show-after">
		<?php include_once('header.php'); ?>
		<div class="container-fluid">
			<?php include_once('home.php'); ?>
		</div>
	</div>
	<input type="hidden" value="<?php print base_url(); ?>" id="root">
	<?php print asset('datasigh/js/datasigh.js') ?>
	<?php print asset('datasigh/js/libs/vue.js') ?>
	<?php print asset('js/app.js') ?>
	<!-- v1.0 -->
</body>
</html>