<center>
  <h1>Home</h1>
</center>

<br>

<center>
  <p>Exemplo de requisição em vuejs. Obtendo dado do arquivo storage/config.ini</p>
  <p>Dados da requisição: <span v-if="loading">...</span> <span v-else>{{response}}</span></p>
  <p>Em loop:</p>
  <p v-for="(row, key) in response" :key="key">
    {{key}} = {{row}}
  </p>
</center>
