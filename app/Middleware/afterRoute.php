<?php

namespace App\Middleware;

use Request;

class AfterRoute
{

  public function handle($next)
  {
    $request = Request::getContent();
    $response = $next;

    if ( is_array($next) || is_object($next)) {
      $json = json_encode($next);
      header('Content-Type: application/json;charset=utf-8');
      return $json;
    }

    header("Content-Type: " . Request::getContentType() . ";charset=utf-8");

    return $response;
  }

}