<?php

namespace App\Exceptions;

class Handler
{

  public function render($e)
  {
    if ( env('APP_ENV') == 'local' ) {
      dd_exception($e);
      dd_exception([
        'code' => $e->getCode(),
        'message' => $e->getMessage(),
        'file' => $e->getFile(),
        'line' => $e->getLine()
      ]);
    }
    echo '<p class="error">Server error!</p>';    
    exit();
  }

}