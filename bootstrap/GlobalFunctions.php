<?php

if (!function_exists('getallheaders')) {
  function getallheaders() {
    $headers = [];
    foreach ($_SERVER as $name => $value) {
      if (substr($name, 0, 5) == 'HTTP_') {
        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
      }
    }
    return $headers;
  }
}

function abort($code, $msg = '') {
  http_response_code($code);
  if ( strpos(request_url(), "/api") !== false ) {
    header('Content-Type: application/json');

    $id = $code;
    if ( $msg != '' ) {
      $msgArray = explode('|', $msg);
      if ( is_numeric($msgArray[0]) ) {
        $id = intval($msgArray[0]);
        $msg = isset($msgArray[1]) ? $msgArray[1] : $msg;
      }
      else $msg = $msgArray[0];
    }

    echo json_encode([
      'error' => [
        'id' => $id,
        'message' => $msg
      ]
    ]);
  }
  exit;
}

function dd($dump) {
  header('Content-Type: text/html;charset=utf-8');
  http_response_code(200);
  echo  "<pre style='background-color: black;color: orange;padding: 0.5rem;'>".
        "[process time]: => ". laravel_end()."\n".
        "[dump]: \n".print_r($dump, true)."</pre>";
  exit;
}

function dd_exception($dump) {
  header('Content-Type: text/html;charset=utf-8');
  http_response_code(500);
  echo  '<body style="padding:0.5rem;margin:0"><pre style="background-color: black;color: #1dd61d;margin:0;padding:0.5rem;border:5px solid red;">'.
        '[process time]: => '. laravel_end()."\n\n".
        "[dump]: \n\n".print_r($dump, true).'</pre></body>';
  exit;
}

function laravel_end()
{
  return substr( "".(microtime(true) - LARAVEL_START)."", 0, 4);
}

function is_closure($t)
{
  return is_object($t) && ($t instanceof Closure);
}

function base_url($path = "/") 
{
  // output: /myproject/index.php
  $currentPath = $_SERVER['PHP_SELF']; 

  // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index ) 
  $pathInfo = pathinfo($currentPath); 

  // output: localhost OR mf.srv.br
  $hostName = $_SERVER['HTTP_HOST']; 

  // output: http:// OR https://
  $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
  if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
    $protocol = 'https';
  }
  else $protocol = 'http';

  $url = $protocol.'://'.str_replace("//", "/", $hostName.$pathInfo['dirname']).$path;
  $url = str_replace('\\', '', $url);
  // return: http://localhost/myproject/
  return $url;
}

function request_url()
{
  $baseUrl = base_url();
  $baseUrl = str_replace("http://", '', $baseUrl);
  $baseUrl = str_replace("https://", '', $baseUrl);
  $baseUrl = str_replace($_SERVER['HTTP_HOST'], '', $baseUrl);

  if ( !$baseUrl ) $baseUrl = '/';

  $request_url = explode('?', $_SERVER['REQUEST_URI']);
  if ( $baseUrl != '/' ) {
    $request_url = str_replace( $baseUrl,'', array_shift($request_url));
  }
  else {
    $request_url = array_shift($request_url);
  }
  if ( !$request_url ) $request_url = '/';
  if ( $request_url[0] != '/' ) $request_url = "/$request_url";

  return $request_url;
}

function view($view)
{
  $view = str_replace('.', '/', $view);
  $dirfile = dirname(__DIR__) . "/resources/views/$view.php";
  if ( !file_exists($dirfile) ) abort(500, "View: '$view' not found.");
  return require_once $dirfile;
}

function asset($slot)
{
  if ( env('APP_ENV') == 'local' ) $min = '';
  else $min = '.min';

  $array_slot = explode('.', $slot);
  $type = array_pop($array_slot);

  date_default_timezone_set('America/Sao_Paulo');
  $version = date ("YmdHis", filemtime("assets/{$slot}"));

  $path = implode('/', $array_slot);
  $path = base_url()."assets/{$path}{$min}.{$type}?v={$version}";

  if ( $type == 'css' ) {
    return '<link rel="stylesheet" href="'.$path.'">';
  }
  else if ( $type == 'js' ) {
    return '<script src="'.$path.'"></script>';
  }
}

// READING .ENV
if(file_exists(dirname(__DIR__)."/.env")) {
  $fn = fopen(dirname(__DIR__)."/.env","r");
  $env = [];
  while(! feof($fn))  {
    $line = fgets($fn);
    if ( $line ) {
      if ( strpos($line, '=') ) {
        $explode = explode('=', $line);
        if ( isset($explode[0]) ) {
          $key = $explode[0];
          $value = (isset($explode[1])) ? $explode[1] : '';
          putenv(trim($key)."=".trim($value));
        }
      }
    }
  }
  fclose($fn);
  if(!function_exists('env')) {
    function env($key, $default = null)
    {
      $value = getenv($key);
      if ($value === false) {
        return $default;
      }
      return $value;
    }
  }
}
else {
  abort(500, 'env');
}