<?php

namespace Bootstrap;

class Connection
{

  private static $selected_connection;
  private static $connections_list;
  private static $connections_set;
  private static $default_connection;
  private static $_instance;

  function __construct()
  {
    $config = require_once dirname(__DIR__)."/config/database.php";
    self::$connections_list = $config['connections'];
    self::$default_connection = $config['default'];
  }

  private static function set($name, $info)
  {
    if ( empty($info['options']) ) {
      $info['options'] = extension_loaded('pdo_mysql') ? array_filter([
        \PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_TIMEOUT => 30
      ]) : [];
    }
    if ( empty($info['charset']) ) {
      $info['charset'] = 'utf8';
    }
    if ( empty($info['collation']) ) {
      $info['collation'] = 'utf8_general_ci';
    }
    if ( empty($info['driver']) ) {
      $info['driver'] = 'mysql';
    }
    if ( empty($info['prefix_indexes']) ) {
      $info['prefix_indexes'] = true;
    }
    if ( empty($info['strict']) ) {
      $info['strict'] = true;
    }
    if ( empty($info['engine']) ) {
      $info['engine'] = null;
    }

    self::$connections_set[$name] = new \PDO("{$info['driver']}:host={$info['host']}; dbname={$info['database']};port={$info['port']};charset={$info['charset']}", "{$info['username']}", "{$info['password']}", $info['options']);
  }

  public static function connection($name)
  {
    self::$selected_connection = $name;
    if (self::$_instance === null) {
      self::$_instance = new self;
    }
    return self::$_instance;
  }

  public static function db()
  {
    if ( self::$selected_connection ) {
      $conn = self::$selected_connection;
    }
    else $conn = self::$default_connection;

    if ( !isset(self::$connections_set[$conn]) ) {
      if ( !isset(self::$connections_list[$conn]) ) abort(500, "Connection unset: $conn");
      self::set($conn, self::$connections_list[$conn]);
    }

    return self::$connections_set[$conn];
  }

  public static function select($query, $params = null)
  {
    $db = self::db();
    if ($params) {
      $array_params = [];
      foreach($params as $key => $q) {
        $array_params[":$key"] = $q;
      }
      $stmt = $db->prepare($query);
      $stmt->execute($array_params);
    }
    else {
      $stmt = $db->prepare($query);
      $stmt->execute();
    }

    $result = [];
    if ( strpos($query, 'SELECT') !== false ) {
      if (!$stmt->fetchObject(__CLASS__)) return null;
      while ($row = $stmt->fetchObject(__CLASS__)) {
        $result[] = $row;
      }
    } else {
      self::$selected_connection = null;
      return $stmt->rowCount();
    }

    self::$selected_connection = null;
    return (object) $result;
  }

  public static function insert($query, $params = null)
  {
    $db = self::db();
    if ($params) {
      $array_params = [];
      foreach($params as $key => $q) {
        $array_params[":$key"] = $q;
      }
      $stmt = $db->prepare($query);
      $stmt->execute($array_params);
    }
    else {
      $stmt = $db->prepare($query);
      $stmt->execute();
    }

    $result = [];
    if ( strpos($query, 'SELECT') !== false ) {
      if (!$stmt->fetchObject(__CLASS__)) return null;
      while ($row = $stmt->fetchObject(__CLASS__)) {
        $result[] = $row;
      }
    } else {
      self::$selected_connection = null;
      return $stmt->rowCount();
    }

    self::$selected_connection = null;
    return (object) $result;
  }

  public static function insertGetId($query, $params = null)
  {
    $db = self::db();
    if ($params) {
      $array_params = [];
      foreach($params as $key => $q) {
        $array_params[":$key"] = $q;
      }
      $stmt = $db->prepare($query);
      $stmt->execute($array_params);
    }
    else {
      $stmt = $db->prepare($query);
      $stmt->execute();
    }
    return $db->lastInsertId();
  }

  public static function update($query, $params = null)
  {
    $db = self::db();
    if ($params) {
      $array_params = [];
      foreach($params as $key => $q) {
        $array_params[":$key"] = $q;
      }
      $stmt = $db->prepare($query);
      $stmt->execute($array_params);
    }
    else {
      $stmt = $db->prepare($query);
      $stmt->execute();
    }

    $result = [];
    if ( strpos($query, 'SELECT') !== false ) {
      if (!$stmt->fetchObject(__CLASS__)) return null;
      while ($row = $stmt->fetchObject(__CLASS__)) {
        $result[] = $row;
      }
    } else {
      self::$selected_connection = null;
      return $stmt->rowCount();
    }

    self::$selected_connection = null;
    return (object) $result;
  }

  public static function delete($query, $params = null)
  {
    $db = self::db();
    if ($params) {
      $array_params = [];
      foreach($params as $key => $q) {
        $array_params[":$key"] = $q;
      }
      $stmt = $db->prepare($query);
      $stmt->execute($array_params);
    }
    else {
      $stmt = $db->prepare($query);
      $stmt->execute();
    }

    $result = [];
    if ( strpos($query, 'SELECT') !== false ) {
      if (!$stmt->fetchObject(__CLASS__)) return null;
      while ($row = $stmt->fetchObject(__CLASS__)) {
        $result[] = $row;
      }
    } else {
      self::$selected_connection = null;
      return $stmt->rowCount();
    }

    self::$selected_connection = null;
    return (object) $result;
  }

}