<?php

namespace Bootstrap;

class Helper
{

  static public function converteData($dt) {
    //Converte de string para Datetime
    $y = substr($dt, 0, 4);
    $m = substr($dt, 4, 2);
    $d = substr($dt, 6, 7);
    return new \DateTime(implode('-', [$y, $m, $d]));
  }

  static public function dateGetWeekday($_datetime)
	{
		switch($_datetime->format('N')){
			case 1:
				$result = 'segunda-feira';
				break;
			case 2:
				$result = 'terça-feira';
				break;
			case 3:
				$result = 'quarta-feira';
				break;
			case 4:
				$result = 'quinta-feira';
				break;
			case 5:
				$result = 'sexta-feira';
				break;
			case 6:
				$result = 'sábado';
				break;
			case 7:
				$result = 'domingo';
				break;
			default:
				$result = false;
		}

		return $result;
  }
  
  static public function dateGetMonth($_datetime)
	{
		switch($_datetime->format('n')){
			case 1:
				$result = 'Janeiro';
				break;
			case 2:
				$result = 'Fevereiro';
				break;
			case 3:
				$result = 'Março';
				break;
			case 4:
				$result = 'Abril';
				break;
			case 5:
				$result = 'Maio';
				break;
			case 6:
				$result = 'Junho';
				break;
			case 7:
				$result = 'Julho';
				break;
			case 8:
				$result = 'Agosto';
				break;
			case 9:
				$result = 'Setembro';
				break;
			case 10:
				$result = 'Outubro';
				break;
			case 11:
				$result = 'Novembro';
				break;
			case 12:
				$result = 'Dezembro';
				break;
			default:
				$result = false;
		}
		return $result;
  }
  
  static public function dateStringToDateTime($_date, $_hour = null)
	{
		if ( strpos($_date, '/') ) {
			$date = explode('/', $_date);
			$y = $date[2];
			$m = $date[1];
			$d = $date[0];
			$result = implode('-', [$y, $m, $d]);
		}
		else if ( strpos($_date, '-') ) {
			$date = explode('-', $_date);
			$y = $date[0];
			$m = $date[1];
			$d = $date[2];
			$result = implode('-', [$y, $m, $d]);
		}
		else {
			$y = substr($_date, 0, 4);
			$m = substr($_date, 4, 2);
			$d = substr($_date, 6, 2);
			$result = implode('-', [$y, $m, $d]);
		}
    if ( $_hour ) $result .= ' '.self::hourAddDot($_hour);
		return new \DateTime($result);
  }
  
  static public function dateGetAge($_date_string)
	{
    $now = new \DateTime();
    $nascimento = self::dateStringToDateTime($_date_string);
    $diff = $nascimento->diff($now);
		return $diff->y;
  }

	static public function hourAddDot($_hour)
	{
		$result = $_hour;
		if ( !strpos($_hour, ':') ) {
			if ( strlen($_hour) === 3 ) $_hour = implode('', ['0', $_hour]);
			$result = implode(':', [substr($_hour, 0, 2), substr($_hour, 2, 2)]);
		}
		return $result;
	}

	static public function hourRemoveDot($_hour)
	{
		if ( strpos($_hour, ':') ) {
			$result = str_replace(':', '', $_hour);
			if ( strlen($result) === 3 ) $result = implode('', ['0', $result]);
		}
		return $result;
	}

  static public function converteHora($hr) {
    if ( strpos($hr, ':') ) {
      $return = str_replace(':', '', $hr);
      if ( strlen($return) === 3 ) $return = implode('', ['0', $return]);
    } else {
      if ( strlen($hr) === 3 ) $hr = implode('', ['0', $hr]);
      $return = implode(':', [substr($hr, 0, 2), substr($hr, 2, 2)]);
    }
    return $return;
  }

  static public function getWeekday($dt) {
    switch($dt->format('N')){
      case 1:
        $return = 'segunda-feira';
        break;
      case 2:
        $return = 'terça-feira';
        break;
      case 3:
        $return = 'quarta-feira';
        break;
      case 4:
        $return = 'quinta-feira';
        break;
      case 5:
        $return = 'sexta-feira';
        break;
      case 6:
        $return = 'sábado';
        break;
      case 7:
        $return = 'domingo';
        break;
      default:
        $return = false;
    }

    return $return;
  }

  static public function diffMinutes($startDateTime, $endDateTime) {
    $diff = $startDateTime->diff($endDateTime);
    $minutes = $diff->days * 24 * 60;
    $minutes += $diff->h * 60;
    $minutes += $diff->i;
    return $minutes;
  }

  static public function makefile($dir, $content, $overwrite = true) {
    $dir = dirname(__DIR__)."/storage/".$dir;
    $parts = explode('/', $dir);
    $file = array_pop($parts);
    $path = implode('/', $parts);
    if (self::makedir($path) || $overwrite) file_put_contents($dir, $content);
    else file_put_contents($dir, PHP_EOL.$content, FILE_APPEND);
  }

  static public function makedir($dir) {
    if (!file_exists($dir)) {
      mkdir($dir, 0777, true);
      return true;
    }
    return false;
  }

  static public function readfile_json($file) {
    $dir = dirname(__DIR__)."/storage/{$file}.json";
    if ( file_exists($dir) ) {
      $json = file_get_contents($dir);
      return json_decode($json);
    }
    return false;
  }

  static public function readfile_ini($file) {
    $dir = dirname(__DIR__)."/storage/{$file}.ini";
    if ( file_exists($dir) ) return parse_ini_file($dir, true);
    return false;
  }

  static public function writefile_ini($dir, $array) {
    $text = '';
    foreach($array as $key => $row)
    {
      if ( is_array($row) )
      {
        $text .= "[{$key}]".PHP_EOL;
        foreach($row as $key2 => $row2) {
          $text .= "{$key2}={$row2}".PHP_EOL;
        }
      }
      else
      {
        $text .= "{$key}={$row}".PHP_EOL;
      }
    }
    self::makefile($dir.".ini", $text);
  }

  static function downloadfile($url){
    $filename = basename($url);
    file_put_contents($filename, file_get_contents($url));
    return (filesize($filename) > 0)? true : false;
  }

  static public function checkOldLogs($dir) {
    $files = glob(dirname(__DIR__)."/storage/logs/{$dir}/*.txt");
    $now = new \DateTime();
    foreach ($files as $path) {
      $filename = explode('/', $path);
      $filename = array_pop($filename);
      $filename = explode('.', $filename);
      $filename = $filename[0];
      $dt_file = new \DateTime($filename);
      $diff = $now->diff($dt_file);
      if ( $diff->days > 7 ) unlink($path);
    }
  }

}
