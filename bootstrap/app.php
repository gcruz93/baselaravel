<?php

spl_autoload_register(function ($class) {
  require_once __DIR__ . "/../$class.php";
});

class_alias('Bootstrap\Request', 'Request');
class_alias('Bootstrap\Connection', 'DB');
class_alias('Bootstrap\Helper', 'Helper');
class_alias('App\Middleware\AfterRoute', 'AfterRoute');
class_alias('App\Exceptions\Handler', 'Exceptions');

class_alias('App\Model', 'Model');

require_once dirname(__DIR__) . '/bootstrap/GlobalFunctions.php';

require_once dirname(__DIR__) . '/bootstrap/Route.php';

require_once dirname(__DIR__) . '/routes/web.php';
Route::$prefix = '/api';
require_once dirname(__DIR__) . '/routes/api.php';

if ( env('APP_DEBUG') ) {
  // set the exception handler
  set_exception_handler(function ($e) {
    $response = (new Exceptions)->render($e);
    exit((new AfterRoute)->handle($response));
  });

  // set the error handler
  set_error_handler(function($code, $message, $file, $line) {
    throw new ErrorException($message, 500, $code, $file, $line);
  },-1);
}

date_default_timezone_set(env('APP_TIMEZONE'));

$response = Route::dispatch();
exit ((new AfterRoute)->handle($response));