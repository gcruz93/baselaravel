<?php

namespace Bootstrap;

class Request
{

  static public function getContent()
  {
    parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $content);
    $post = @json_decode(file_get_contents('php://input'));
    if ( $post ) {
      foreach ( $post as $key => $param ) {
        $content[$key] = $param;
      }
    }
    return (object) $content;
  }

  static public function getContentType()
  {
    $allHeaders = getallheaders();
    return isset($allHeaders['Content-Type']) ? $allHeaders['Content-Type'] : 'text/html';
  }

  static public function getHeaders()
  {
    return getallheaders();
  }

  static public function isJson()
  {
    if ( strpos(self::getContentType(), 'json') !== false ) return true;
    return false;
  }

  static public function ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
      $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
      $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
      $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
      $ipaddress = getenv('REMOTE_ADDR');
    else
      $ipaddress = 'UNKNOWN';

    return $ipaddress; 
  }

  static public function method($compare = null) {
    if ( $compare )
    {
      return $_SERVER['REQUEST_METHOD'] === $compare;
    }
    return $_SERVER['REQUEST_METHOD'];
  }

}