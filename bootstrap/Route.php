<?php

class Route {

  static private $routes = [];
  static public $prefix = '';

  static public function dispatch()
  {
    $request_url = request_url();

    foreach ( self::$routes as $route ) {
      if ( $request_url != $route['path'] ) continue;
      if ( $_SERVER['REQUEST_METHOD'] != $route['method'] ) abort(405);
      $callback = $route['callback'];
      if (!is_closure($callback)) {
        $controller = explode('@', $callback);
        $className = "\\App\\$controller[0]";
        $class = new $className;
        $request = Request::getContent();
        return $class->{$controller[1]}($request);
      } else {
        return call_user_func($callback);
      }
    }
    abort(404);
  }

  static private function add_route($route)
  {
    $route['path'] = str_replace(' ', '', $route['path']);
    if ( $route['path'] == '' || $route['path'][0] != '/' ) $route['path'] = '/'.$route['path'];
    $route['path'] = self::$prefix . $route['path'];
    self::$routes[] = $route;
  }

  static public function get($path, $callback)
  {
    self::add_route([
      'path' => $path,
      'callback' => $callback,
      'method' => 'GET'
    ]);
  }

  static public function put($path, $callback)
  {
    self::add_route([
      'path' => $path,
      'callback' => $callback,
      'method' => 'PUT'
    ]);
  }

  static public function post($path, $callback)
  {
    self::add_route([
      'path' => $path,
      'callback' => $callback,
      'method' => 'POST'
    ]);
  }

  static public function delete($path, $callback)
  {
    self::add_route([
      'path' => $path,
      'callback' => $callback,
      'method' => 'DELETE'
    ]);
  }

  static public function patch($path, $callback)
  {
    self::add_route([
      'path' => $path,
      'callback' => $callback,
      'method' => 'PATCH'
    ]);
  }

}
