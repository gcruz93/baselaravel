let root = document.getElementById('root').value;
document.getElementById('root').remove();

const app = new Vue({
  el: '#app',
  data: {
    url_api: `${root}api`,
    loading: false,
    response: {}
  },
  methods: {
    start() {
      this.fetchData();
    },
    fetchData() {
      return new Promise(async (resolve, reject) => {
        this.loading = true;
        const res = await _ds._fetch(`${this.url_api}/fetch`);
        this.loading = false;
        if (!res.error) this.response = res;
        else {
          _ds.alert({ 'type': 'error', 'message': res.error.message });
        }
        resolve();
      });
    }
  },
  mounted() {
    this.$nextTick(() => {
      _ds.fadeIn(document.getElementsByClassName('ds-show-after')[0]);
      this.start();
    });
  }
});

delete root;