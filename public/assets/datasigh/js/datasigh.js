const _ds = {
  timeout_alerts: null,
  onload() {
    if (document.body.classList.contains('with-header-onscroll')) {
      window.addEventListener('scroll', e => {
        this.toggle.showHeaderOnScroll();
      });
    }

    document.addEventListener('click', e => {
      const target = e.target;
      if (target.classList.contains('toggleFullScreen')) {
        this.toggle.fullScreen();
      }
      if (target.classList.contains('toggleLeftMenu')) {
        this.toggle.leftMenu();
      }
    }, false);
  },
  togglePassword() {
    $el = document.querySelector('.input-group.password-showable input[name="password"]');
    if ( $el ) {
      if ( $el.type == 'password' ) {
        event.target.classList.remove('fa-eye');
        event.target.classList.add('fa-eye-slash');
        $el.type = 'text';
      }
      else {
        event.target.classList.remove('fa-eye-slash');
        event.target.classList.add('fa-eye');
        $el.type = 'password';
      }
    }
  },

  input: {
    nome() {
      const $el = document.querySelector('.ds-form input[name="nome"]');
      if (!$el) {
        console.log('_ds: elemento inexistente.');
        return false;
      }
      let newValue = $el.value;
      if (newValue.length > 255 ) {
        newValue = newValue.slice(0, 255);
      }
      newValue = newValue.replace(/[^a-zA-ZÀ-ž ]/g, '');
      $el.value = newValue;
    },
    nascimento() {
      const $el = document.querySelector('.ds-form input[name="nascimento"]');
      if (!$el) {
        console.log('_ds: elemento inexistente.');
        return false;
      }
      let newValue = $el.value;
      if (newValue.length > 10 ) {
        newValue = newValue.slice(0, 10);
      }
      newValue = newValue.replace(/\D/g, "");
      newValue = newValue.replace(/(\d{2})(\d)/, "$1/$2");
      newValue = newValue.replace(/(\d{2})(\d)/, "$1/$2");
      if ( newValue.length == 2
          || newValue.length == 5
          || newValue.length == 10 ) {
        if ( !_ds.validate.nascimento(newValue) ) {
          $el.classList.add('invalid');
        }
        else $el.classList.remove('invalid');
      }
      else if (newValue.length == 0) $el.classList.remove('invalid');
      $el.value = newValue;
    },
    cpf() {
      const $el = document.querySelector('.ds-form input[name="cpf"]');
      if (!$el) {
        console.log('_ds: elemento inexistente.');
        return false;
      }
      let newValue = $el.value;
      if (newValue.length > 14 ) {
        newValue = newValue.slice(0, 14);
      }
      newValue = newValue.replace(/\D/g, "");
      newValue = newValue.replace(/(\d{3})(\d)/, "$1.$2");
      newValue = newValue.replace(/(\d{3})(\d)/, "$1.$2");
      newValue = newValue.replace(/(\d{3})(\d{1,5})$/, "$1-$2");
      if ( newValue.length == 14 ) {
        if ( !_ds.validate.cpf(newValue) ) {
          $el.classList.add('invalid');
        }
        else $el.classList.remove('invalid');
      }
      else if (newValue.length == 0) $el.classList.remove('invalid');
      $el.value = newValue;
    },
    celular() {
      const $el = document.querySelector('.ds-form input[name="celular"]');
      if (!$el) {
        console.log('_ds: elemento inexistente.');
        return false;
      }
      let newValue = $el.value;
      if (newValue.length > 15 ) {
        newValue = newValue.slice(0, 15);
      }
      newValue = newValue.replace(/\D/g, "");
      newValue = newValue.replace(/^(\d\d)(\d)/g, "($1) $2");
      newValue = newValue.replace(/(\d{5})(\d)/, "$1-$2");
      if (newValue.length == 15) {
        if (!_ds.validate.celular(newValue)) {
          $el.classList.add('invalid');
        }
        else $el.classList.remove('invalid');
      }
      else if (newValue.length == 0) $el.classList.remove('invalid');
      $el.value = newValue;
    },
    telefone() {
      const $el = document.querySelector('.ds-form input[name="telefone"]');
      if (!$el) {
        console.log('_ds: elemento inexistente.');
        return false;
      }
      let newValue = $el.value;
      if (newValue.length > 14 ) {
        newValue = newValue.slice(0, 14);
      }
      newValue = newValue.replace(/\D/g, "");
      newValue = newValue.replace(/^(\d\d)(\d)/g, "($1) $2");
      newValue = newValue.replace(/(\d{4})(\d)/, "$1-$2");
      if (newValue.length == 14) {
        if (!_ds.validate.telefone(newValue)) {
          $el.classList.add('invalid');
        }
        else $el.classList.remove('invalid');
      }
      else if (newValue.length == 0) $el.classList.remove('invalid');
      $el.value = newValue;
    },
    async cep() {
      const $el = document.querySelector('.ds-form input[name="cep"]');
      if (!$el) {
        console.log('_ds: elemento inexistente.');
        return false;
      }
      let newValue = $el.value;
      if (newValue.length > 9 ) {
        newValue = newValue.slice(0, 9);
      }
      newValue = newValue.replace(/\D/g, "");
      newValue = newValue.replace(/(\d{5})(\d{1,3})$/, "$1-$2");
      $el.value = newValue;
      if ( newValue.length == 9 ) {
        $el.setAttribute("disabled", true);
        if (document.querySelector('.ds-form input[name="bairro"]')) {
          $el_bairro = document.querySelector('.ds-form input[name="bairro"]');
          $el_bairro.value = '...';
          $el_bairro.setAttribute("disabled", true);
        }
        if (document.querySelector('.ds-form input[name="cidade"]')) {
          $el_cidade = document.querySelector('.ds-form input[name="cidade"]');
          $el_cidade.value = '...';
          $el_cidade.setAttribute("disabled", true);
        }
        if (document.querySelector('.ds-form input[name="endereco"]')) {
          $el_endereco = document.querySelector('.ds-form input[name="endereco"]');
          $el_endereco.value = '...';
          $el_endereco.setAttribute("disabled", true);
        }
        if (document.querySelector('.ds-form input[name="estado"]')) {
          $el_estado = document.querySelector('.ds-form input[name="estado"]');
          $el_estado.value = '';
          $el_estado.setAttribute("disabled", true);
        }
        if (document.querySelector('.ds-form select[name="estado"]')) {
          $el_estado = document.querySelector('.ds-form select[name="estado"]');
          $el_estado.value = '';
          $el_estado.setAttribute("disabled", true);
        }
        const res = await _ds.api.viacep(newValue);
        $el.disabled = false;
        if (!res || res.erro) {
          _ds.alert({
            'type': 'error',
            'message': 'CEP não encontrado.'
          });
        }
        if (document.querySelector('.ds-form input[name="bairro"]')) {
          $el_bairro = document.querySelector('.ds-form input[name="bairro"]');
          $el_bairro.disabled = false;
          if (res.erro) $el_bairro.value = '';
          else $el_bairro.value = res.bairro;
        }
        if (document.querySelector('.ds-form input[name="cidade"]')) {
          $el_cidade = document.querySelector('.ds-form input[name="cidade"]');
          $el_cidade.disabled = false;
          if (res.erro) $el_cidade.value = '';
          else $el_cidade.value = res.localidade;
        }
        if (document.querySelector('.ds-form input[name="endereco"]')) {
          $el_endereco = document.querySelector('.ds-form input[name="endereco"]');
          $el_endereco.disabled = false;
          if (res.erro) $el_endereco.value = '';
          else $el_endereco.value = res.logradouro;
        }
        if (document.querySelector('.ds-form input[name="estado"]')) {
          $el_estado = document.querySelector('.ds-form input[name="estado"]');
          $el_estado.disabled = false;
          if (res.erro) $el_estado.value = '';
          else $el_estado.value = res.uf;
        }
        if (document.querySelector('.ds-form select[name="estado"]')) {
          $el_estado = document.querySelector('.ds-form select[name="estado"]');
          $el_estado.disabled = false;
          if (res.erro) $el_estado.value = '';
          else $el_estado.value = res.uf;
        }
      }
    }
  },
  alert(info = null) {
    if ( !info ) {
      console.log('_ds: alert sem informação.');
    }
    if (!('time' in info)) info.time = 5;
    if (!('type' in info)) info.type = '';
    if (!('message' in info)) info.message = '';
    this.clearAlerts();
    document.getElementById('app').insertAdjacentHTML('beforeend', `<div class="ds-alert active-${info.time} ${info.type}">${info.message}</div>`);
    clearTimeout(this.timeout_alerts);
    this.timeout_alerts = setTimeout(e=>{
      const $el = document.querySelector('.ds-alert');
      if ($el) $el.parentNode.removeChild($el);
    }, info.time * 1000);
  },
  clearAlerts() {
    document.querySelectorAll('.ds-alert').forEach(e=>e.remove());
  },
  _fetch(url, info = {}) {
    return new Promise(async function (resolve, reject) {
      let _fetch, csrf = '';

      if (document.querySelector('input[name="_token"]')) {
        csrf = document.querySelector('input[name="_token"]').value;
      }

      if ( !(info.method) ) {
        if ('data' in info) info.method = 'POST';
        else info.method = 'GET';
      }

      if ( !('data' in info) ) info.data = '';

      let data = {
        headers: new Headers({
          'X-CSRF-TOKEN': csrf
        }),
        method: info.method,
        body: JSON.stringify(info.data)
      }

      if ( data.body == '""' ) delete data.body;

      _fetch = await fetch(url, data).catch(error => {
        console.log(error);
      });

      if ( _fetch.status === 419 || _fetch.status === 401 ) {
        location.reload();
      }
      const res = await _fetch.json();
      resolve(res);
    });
  },
  loader: {
    add(target) {
      target.classList.add('with-loader');
    },
    addDisabled(target) {
      target.classList.add('with-loader-disabled');
    },
    remove(target) {
      target.classList.remove('with-loader');
      target.classList.remove('with-loader-disabled');
    },
  },
  date: {
    monthName(value) {
      const month = value instanceof Date ? value.getMonth() + 1 : value;
      switch (month) {
        case 1: return 'Janeiro';
        case 2: return 'Fevereiro';
        case 3: return 'Março';
        case 4: return 'Abril';
        case 5: return 'Maio';
        case 6: return 'Junho';
        case 7: return 'Julho';
        case 8: return 'Agosto';
        case 9: return 'Setembro';
        case 10: return 'Outubro';
        case 11: return 'Novembro';
        case 12: return 'Dezembro';
        default: return '';
      }
    },
    dayName(value) {
      const day = value instanceof Date ? value.getDay() : value;
      switch (day) {
        case 0: return 'Segunda-feira';
        case 1: return 'Terça-feira';
        case 2: return 'Quarta-feira';
        case 3: return 'Quinta-feira';
        case 4: return 'Sexta-feira';
        case 5: return 'Sábado';
        case 6: return 'Domingo';
        default: return '';
      }
    },
    fromString(str, datetime = false) {
      str = `${str}`;
      const y = str.substring(0, 4);
      const m = str.substring(4, 6);
      const d = str.substring(6, 8);
      if (datetime) return new Date(`${y}-${m}-${d} 00:00:00`);
      return `${y}-${m}-${d}`;
    },
    fromStringToObject(str) {
      str = `${str}`;
      const y = str.substring(0, 4);
      const m = str.substring(4, 6);
      const d = str.substring(6, 8);
      return {y, m, d}
    },
    getAge(birthday) {
      const ageDifMs = Date.now() - birthday.getTime();
      const ageDate = new Date(ageDifMs);
      return Math.abs(ageDate.getUTCFullYear() - 1970);
    },
    getType(date) {
      if ( date instanceof Date ) return 'datetime';
      else if ( this.isNumber(date) ) return 'integer';
      return 'string';
    },
    now() {
      return new Date();
    }
  },
  isNumber(value) {
    return /^\d+$/.test(value);
  },

  fadeOut($target, time = 150) {
    _ds.fade($target, time, 100, 0);
  },
  fadeIn($target, time = 150) {
    $target.style.display = 'block';
    _ds.fade($target, time, 0, 100);
  },
  fade($target, time, ini, fin) {
    if (!$target) return;
    var alpha = ini;
    var inc;
    if (fin >= ini) {
      inc = 2;
    } else {
      inc = -2;
    }
    timer = time / 50;
    var i = setInterval(
      function () {
        if ((inc > 0 && alpha >= fin) || (inc < 0 && alpha <= fin)) {
          clearInterval(i);
          if (fin < ini) {
            $target.style.display = 'none';
          }
        }
        _ds.setAlpha($target, alpha);
        alpha += inc;
      }, timer);
  },
  setAlpha($target, alpha) {
    $target.style.filter = "alpha(opacity=" + alpha + ")";
    $target.style.opacity = alpha / 100;
  },
  hide(elem) {
    if (elem) {
      elem.style.opacity = 0;
      elem.style.display = 'none';
    }
  },
  fixHour(str) {
    str = `${str}`;
    if (str.length == 3) str = '0' + str;
    const h = str.substring(0, 2);
    const m = str.substring(2, 4);
    return `${h}:${m}`;
  },
  debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this, args = arguments;
      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  },
  getOffset(target) {
    const rect = target.getBoundingClientRect();
    return {
      top: rect.top - document.body.scrollTop,
      left: rect.left - document.body.scrollTop
    }
  },
  removeClass(query, className) {
    document.querySelectorAll(query).forEach(e => {
      e.classList.remove(className);
    })
  },
  addClass(query, className) {
    document.querySelectorAll(query).forEach(e => {
      e.classList.add(className);
    })
  },
  estados_uf: {
    'Acre': 'AC',
    'Alagoas': 'AL',
    'Amapá': 'AP',
    'Amazonas': 'AM',
    'Bahia': 'BA',
    'Ceará': 'CE',
    'Distrito Federal': 'DF',
    'Espírito Santo': 'ES',
    'Goiás': 'GO',
    'Maranhão': 'MA',
    'Mato Grosso': 'MT',
    'Mato Grosso do Sul': 'MS',
    'Minas Gerais': 'MG',
    'Pará': 'PA',
    'Paraíba': 'PB',
    'Paraná': 'PR',
    'Pernambuco': 'PE',
    'Piauí': 'PI',
    'Rio de Janeiro': 'RJ',
    'Rio Grande do Norte': 'RN',
    'Rio Grande do Sul': 'RS',
    'Rondônia': 'RO',
    'Roraima': 'RR',
    'Santa Catarina': 'SC',
    'São Paulo': 'SP',
    'Sergipe': 'SE',
    'Tocantins': 'TO'
  },
  uf_estados: {
    'AC': 'Acre',
    'AL': 'Alagoas',
    'AP': 'Amapá',
    'AM': 'Amazonas',
    'BA': 'Bahia',
    'CE': 'Ceará',
    'DF': 'Distrito Federal',
    'ES': 'Espírito Santo',
    'GO': 'Goiás',
    'MA': 'Maranhão',
    'MT': 'Mato Grosso',
    'MS': 'Mato Grosso do Sul',
    'MG': 'Minas Gerais',
    'PA': 'Pará',
    'PB': 'Paraíba',
    'PR': 'Paraná',
    'PE': 'Pernambuco',
    'PI': 'Piauí',
    'RJ': 'Rio de Janeiro',
    'RN': 'Rio Grande do Norte',
    'RS': 'Rio Grande do Sul',
    'RO': 'Rondônia',
    'RR': 'Roraima',
    'SC': 'Santa Catarina',
    'SP': 'São Paulo',
    'SE': 'Sergipe',
    'TO': 'Tocantins'
  },
  toggle: {
    fullScreen() {
      var doc = window.document;
      var docEl = doc.documentElement;
      var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
      var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;
      if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) requestFullScreen.call(docEl);
      else cancelFullScreen.call(doc);
    },
    leftMenu() {
      const $el = document.querySelector('.ds-main-left');
      $el.classList.toggle('active');
    },
    showHeaderOnScroll() {
      const $header = document.querySelector('.ds-header');
      const distTop = document.documentElement.scrollTop;

      if (distTop >= 54) $header.classList.add('with-scroll');
      else {
        if ( window.innerWidth > 992 ) {
          if (distTop <= 1) $header.classList.remove('with-scroll');
        } else {
          if (distTop <= 54) {
            if ($header.classList.contains('with-middle')) {
              $header.classList.remove('with-scroll');
            } else if (distTop <= 1) $header.classList.remove('with-scroll');
          }
        }
      }
    }
  },
  form: {
    serialize(form) {
      /*!
      * Serialize all form data into a query string
      * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
      * @param  {Node}   form The form to serialize
      * @return {String}      The serialized form data
      */

        // Setup our serialized data
        var serialized = [];

        // Loop through each field in the form
        for (var i = 0; i < form.elements.length; i++) {

          var field = form.elements[i];

          // Don't serialize fields without a name, submits, buttons, file and reset inputs, and disabled fields
          if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;

          // If a multi-select, get all selections
          if (field.type === 'select-multiple') {
            for (var n = 0; n < field.options.length; n++) {
              if (!field.options[n].selected) continue;
              serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[n].value));
            }
          }

          // Convert field data to a query string
          else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
            serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value));
          }
        }
        return serialized.join('&');
    },
    getData(form) {
      var formData = {};
      if (typeof form === 'string' || form instanceof String) form = document.forms[form];
      if (!form) {
        console.log('_ds: elemento inexistente.');
        return false;
      }
      for (var i = 0; i < form.elements.length; i++) {
        var field = form.elements[i];
        if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;
        if (field.type === 'select-multiple') {
          for (var n = 0; n < field.options.length; n++) {
            if (!field.options[n].selected) continue;
            formData[encodeURIComponent(field.name)] = field.options[n].value;
          }
        }
        else if ( field.type == 'checkbox' ) {
          if ( field.checked ) {
            formData[encodeURIComponent(field.name)] = true;
          }
          else {
            formData[encodeURIComponent(field.name)] = false;
          }
        }
        else if ( field.type == 'radio' ) {
          if ( field.checked ) {
            formData[encodeURIComponent(field.name)] = field.value;
          }
          else {
            formData[encodeURIComponent(field.name)] = '';
          }
        }
        else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
          formData[encodeURIComponent(field.name)] = field.value;
        }
      }
      return formData;
    },
    checkData(formData) {
      let allow = true;
      let message = 'Por favor, corrija os campos inválidos.';
      Object.keys(formData).forEach(e=>{
        const value = formData[e];
        let $el = document.querySelector(`form input[name="${e}"]`);

        if ( $el ) {
          $el.classList.remove('invalid');
          let validate = false;
          if ($el.attributes["required"] == "true") validate = true;
          else if (value) validate = true;
          if (validate) {
            switch(e) {
              case 'nome':
                if (value.length < 3) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'Nome inválido.';
                }
              break;
              case 'nascimento':
                if (value.length != 10) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'Data de nascimento inválida.';
                }
              break;
              case 'password':
                if (value.length < 6) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'Senha muito pequena.';
                }
              break;
              case 'password2':
                $el2 = document.querySelector('form input[name="password"]');
                value2 = formData['password'];
                if (value.length < 6) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'Senha muito pequena.';
                }
                if (value != value2) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'Senhas diferentes.';
                }
              break;
              case 'cpf':
                if (!_ds.validate.cpf(value)) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'CPF inválido.';
                }
              break;
              case 'cep':
                if (!_ds.validate.cep(value)) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'CEP inválido.';
                }
              break;
              case 'estado':
                if (value.length < 4) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'Estado inválido.';
                }
              break;
              case 'bairro':
                if (value.length < 4) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'Bairro inválido.';
                }
              break;
              case 'cidade':
                if (value.length < 4) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'Cidade inválida.';
                }
              break;
              case 'endereco':
                if (value.length < 4) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'Endereço inválido.';
                }
              break;
              case 'celular':
                if (!_ds.validate.celular(value)) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'Celular inválido.';
                }
              break;
              case 'telefone':
                if (!_ds.validate.telefone(value)) {
                  $el.classList.add('invalid');
                  allow = false;
                  message = 'Telefone inválido.';
                }
              break;
              default:
            }
          }
        }
      });
      if ( !allow ) {
        $invalids = document.querySelectorAll('input.invalid');
        $last_invalid = $invalids[$invalids.length-1];
        if (!$last_invalid) {
          console.log('_ds: elemento inexistente.');
          return false;
        }
        $last_invalid.scrollIntoView();
        window.scrollBy(0, -70);
        $last_invalid.focus();
        _ds.alert({
          'type': 'error',
          'message': message
        });
      }
      return allow;
    },
    clearErrors(formData) {
      Object.keys(formData).forEach(e => {
        $el = document.querySelector('form input[name="'+e+'"]');
        if ($el) $el.classList.remove('invalid');
      });
    },
    setData(form, data) {
      if (typeof form === 'string' || form instanceof String){
        form = document.forms[form];
      }
      if (!form) {
        console.log('_ds: elemento inexistente.');
        return false;
      }
      for (var i = 0; i < form.elements.length; i++) {
        var field = form.elements[i];
        if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;
        for (key in data) {
          const value = data[key];
          if(field.name == key) {
            form.elements[key].value = value;
          }
        }
      }
    },
    start() {
      if (document.querySelector('.ds-form input[name="nome"]')) {
        $el = document.querySelector('.ds-form input[name="nome"]');
        $el.addEventListener("input", function () { _ds.input.nome(); });
      }
      if (document.querySelector('.ds-form input[name="cpf"]')) {
        $el = document.querySelector('.ds-form input[name="cpf"]');
        $el.placeholder = "___.___.__-__";
        $el.addEventListener("input", function () { _ds.input.cpf(); });
      }
      if (document.querySelector('.ds-form input[name="celular"]')) {
        $el = document.querySelector('.ds-form input[name="celular"]');
        $el.placeholder = "(__) _____-____";
        $el.addEventListener("input", function () { _ds.input.celular(); });
      }
      if (document.querySelector('.ds-form input[name="telefone"]')) {
        $el = document.querySelector('.ds-form input[name="telefone"]');
        $el.placeholder = "(__) ____-____";
        $el.addEventListener("input", function () { _ds.input.telefone(); });
      }
      if (document.querySelector('.ds-form input[type="tel"][name="nascimento"]')) {
        $el = document.querySelector('.ds-form input[type="tel"][name="nascimento"]');
        $el.placeholder = "dd/mm/aaaa";
        $el.addEventListener("input", function () { _ds.input.nascimento(); });
      }
      if (document.querySelector('.ds-form input[type="date"][name="nascimento"]')) {
        $el = document.querySelector('.ds-form input[type="date"][name="nascimento"]');
        $el.max = new Date().toISOString().split("T")[0];
        $el.min = '1900-01-01';
      }
      if (document.querySelector('.ds-form input[name="cep"]')) {
        $el = document.querySelector('.ds-form input[name="cep"]');
        $el.placeholder = "_____-___";
        $el.addEventListener("input", function () { _ds.input.cep(); });
      }
      if (document.querySelector('.ds-form .password-showable')) {
        $el = document.querySelector('.ds-form .password-showable');
        $el.insertAdjacentHTML('beforeend', `<small class="input-helper-password"><i class="fas fa-eye"></i></small>`);
        $el2 = document.querySelector('.ds-form .input-helper-password');
        $el2.setAttribute("onclick", "_ds.togglePassword()");
      }
    }
  },
  sort: {
    int(property) {
      var sortOrder = 1;
      if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
      }
      return function(a, b) {
        if (sortOrder == -1) {
          return b[property] - a[property];
        } else {
          return a[property] - b[property];
        }
      }
    },
    str(property) {
      var sortOrder = 1;
      if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
      }
      return function (a, b) {
        if (sortOrder == -1) {
          return b[property].localeCompare(a[property]);
        } else {
          return a[property].localeCompare(b[property]);
        }
      }
    }
  },
  validate: {
    email(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },
    cpf(cpf) {
      cpf = cpf.replace(/[^\d]+/g, '');
      if (cpf == '') return false;
      if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999")
        return false;
      add = 0;
      for (i = 0; i < 9; i++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
      rev = 11 - (add % 11);
      if (rev == 10 || rev == 11)
        rev = 0;
      if (rev != parseInt(cpf.charAt(9)))
        return false;
      add = 0;
      for (i = 0; i < 10; i++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
      rev = 11 - (add % 11);
      if (rev == 10 || rev == 11)
        rev = 0;
      if (rev != parseInt(cpf.charAt(10)))
        return false;
      return true;
    },
    cep(cep) {
      cep = cep.replace(/[^\d]+/g, '');
      if (cep == '') return false;
      if (cep.length != 8 ||
        cep == "00000000")
        return false;
      return cep;
    },
    telefone(tel) {
      tel = tel.replace(/[^\d]+/g, '');
      const tel_length = tel.length;
      let dig1;
      if (tel_length == 8) {
        dig1 = tel.substring(0, 1);
      }
      else if (tel_length == 10) {
        dig1 = tel.substring(2, 3);
      }
      else return false;
      if (dig1 > 6 || dig1 == 0) return false;
      return true;
    },
    celular(cel) {
      cel = cel.replace(/[^\d]+/g, '');
      const cel_length = cel.length;
      let dig1;
      if (cel_length == 9) {
        dig1 = cel.substring(0, 1);
      }
      else if (cel_length == 11) {
        dig1 = cel.substring(2, 3);
      }
      else return false;
      if (dig1 < 7) return false;
      return dig1;
    },
    nascimento(data) {
      const now = new Date();
      const year = now.getFullYear();
      const data_array = data.split('/');

      let monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

      switch(data_array.length) {
        case 1:
          if (data_array[0] <= 31) return true;
        break;
        case 2:
          if (data_array[0] <= 31
            && data_array[0] <= monthLength[data_array[1] - 1]
            && data_array[1] <= 12) return true;
        break;
        case 3:
          if (data_array[0] <= 31
            && data_array[0] <= monthLength[data_array[1] - 1]
            && data_array[1] <= 12
            && data_array[2] > 1900
            && data_array[2] <= year) return true;
        break;
        default:
      }
      return false;
    }
  },
  api: {
    async viacep(cep) {
      return new Promise(async function(resolve, reject) {
        cep = _ds.validate.cep(cep);
        if (!cep) resolve();
        const _fetch = await fetch(`https://viacep.com.br/ws/${cep}/json/`).catch(error => console.log(error));
        const res = await _fetch.json();
        resolve(res);
      });
    }
  }
}
window.addEventListener('DOMContentLoaded', _ds.onload(), false);
