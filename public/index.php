<?php
/**
 * gccruz93@gmaill.com
 * 1.0 - 09/08/2019
 */

ob_start();
session_start();

define('LARAVEL_START', microtime(true));

$uri = urldecode(
  parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
);

if ($uri !== '/' && file_exists(__DIR__.'/'.$uri)) {
  return false;
}

require_once dirname(__DIR__) . '/bootstrap/App.php';
